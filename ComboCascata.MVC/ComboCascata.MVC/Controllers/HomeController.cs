﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComboCascata.MVC.Controllers
{
    public class HomeController : Controller
    {
        private List<PaisViewModel> _listaPais = new List<PaisViewModel>()
        {
            new PaisViewModel() { Id = 1, Nome= "Brasil" },
            new PaisViewModel() { Id = 2, Nome= "Estados Unidos" },
            new PaisViewModel() { Id = 3, Nome= "Canadá" }
        };

        private List<EstadoViewModel> _listaEstados = new List<EstadoViewModel>()
        {
            new EstadoViewModel() { Id =1, Nome = "Rio de Janeiro", IdPais=1 },
            new EstadoViewModel() { Id =2, Nome = "São Paulo", IdPais=1 },
            new EstadoViewModel() { Id =3, Nome = "Minas Gerais", IdPais=1 },
            new EstadoViewModel() { Id =4, Nome = "Ceará", IdPais=1 },
            new EstadoViewModel() { Id =5, Nome = "Espirito Santo", IdPais=1 },
            new EstadoViewModel() { Id =6, Nome = "Manaus", IdPais=1 },
            new EstadoViewModel() { Id =7, Nome = "New York", IdPais=2 },
            new EstadoViewModel() { Id =8, Nome = "Washington", IdPais=2 },
            new EstadoViewModel() { Id =9, Nome = "Québec", IdPais=3 },
            new EstadoViewModel() { Id =10, Nome = "Colúmbia", IdPais=3 },
            new EstadoViewModel() { Id =11, Nome = "Ontário", IdPais=3 },
        };



        public ActionResult Index()
        {
            var ret = new List<PaisViewModel>();
            ret.AddRange(_listaPais);
            ret.Insert(0, new PaisViewModel() { Id = -1, Nome = "--" });

            return View(ret);
        }

        [HttpPost]
        public ActionResult ObterEstados(int idPais)
        {
            var list = _listaEstados.FindAll(p => p.IdPais == idPais);
            return Json(list);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}